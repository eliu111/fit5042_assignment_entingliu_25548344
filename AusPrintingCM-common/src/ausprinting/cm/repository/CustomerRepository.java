package ausprinting.cm.repository;

import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Remote;

import ausprinting.cm.repository.entities.ContactPerson;
import ausprinting.cm.repository.entities.Customer;
import ausprinting.cm.repository.entities.Industry;

/**
 * 
 * The interface defines the common behaviors across all customer repository implementation
 * 
 * @author EnTing
 *
 */

@Remote
public interface CustomerRepository {
	
	
	/**
	 * Add the customer being passed as parameter into the repository
	 * 
	 * @param customer
	 * @throws Exception
	 */
	
	public void addCustomer(Customer customer) throws Exception;
	
	
	/**
	 * Search for a customer by its customer ID
	 * 
	 * @param id the customerId of the customer to search for
	 * @return the customer found
	 * @throws Exception
	 */
	
	public Customer searchCustomerById(int id) throws Exception;
	
	
	/**
	 * Return all the customers in the repository
	 * 
	 * @return all the customers in the repository
	 * @throws Exception
	 */
	
	public List<Customer> getAllCustomers() throws Exception;
	
	
	/**
	 * Remove the customer, whose customer ID matches the one being passed as
     * parameter, from the repository
	 * 
	 * @param customerId
	 */
	
	public void removeCustomer(int customerId) throws Exception;
	
	/**
	 * Update a customer in the repository
	 * 
	 * @param customer
	 * @throws Exception
	 */
	
	public void editCustomer(Customer customer) throws Exception;
	
	
	/**
	 * Search for customers by the input industry type id
	 * 
	 * @param id
	 * @return the customer found by type of the industry
	 * @throws Exception
	 */
	
	// ContactPerson methods
	
	public Set<Customer> searchCustomerByContactPerson(ContactPerson contactPerson) throws Exception;
	
	
	public void addContactPerson(ContactPerson contactPerson) throws Exception; 
	
	
	public ContactPerson searchContactPersonById(int id) throws Exception;
	
	
	public void editContactPerson(ContactPerson contactPerson) throws Exception;
	
	
	public void removeContactPerson(int contactPersonId) throws Exception;
	
	
	/**
	 * Return all the contact people in the repository
	 * 
	 * @return all Customer Contact(staff) in the repository
	 * @throws Exception
	 */
	public List<ContactPerson> getAllContactPeople() throws Exception;
	
	
	// Industry methods
	public void addIndustry(Industry industry) throws Exception; 
	
	
	public Industry searchIndustryById(int id) throws Exception;
	
	
	public void editIndustry(Industry industry) throws Exception;
	
	
	public void removeIndustry(int industryId) throws Exception;
	
	
	public List<Industry> getAllIndustries() throws Exception;
	
	
	public Set<Customer> searchCustomerByIndustry(Industry industry) throws Exception;
	
	public List<Customer> searchCustomerByName(String name) throws Exception;
}
