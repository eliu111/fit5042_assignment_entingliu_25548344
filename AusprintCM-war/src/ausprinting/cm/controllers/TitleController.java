package ausprinting.cm.controllers;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.faces.context.FacesContext;


/**
 * 
 * @author EnTing
 *
 */

@Named(value = "titleController")
@RequestScoped
public class TitleController {
	private String pageTitle;
	
	public TitleController() {
		// Set the page title
		pageTitle = "AusPrinting Customer Management";
	}
	
	public String getPageTitle() {
		return pageTitle;
	}
	
	public void setPageTitle() {
		this.pageTitle = pageTitle;
	}
}
