package ausprinting.cm.repository;

import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ausprinting.cm.repository.CustomerRepository;
import ausprinting.cm.repository.entities.ContactPerson;
import ausprinting.cm.repository.entities.Customer;
import ausprinting.cm.repository.entities.Industry;

/**
 * 
 * This class implements the CustomerRepository class.
 * 
 * @author EnTing
 *
 */

@Stateless
public class JPACustomerRepositoryImpl implements CustomerRepository{

	
	@PersistenceContext (unitName = "AusPrinting-ejbPU")
	private EntityManager entityManager;
	
//	@PostConstruct
//	public void init() {
//		Customer customer = new Customer();
//		customer.setCustomerId(1);
//		searchCustomerById(customer.getCustomerId());
//		
//		ContactPerson contactPerson = new ContactPerson();
//		contactPerson.setContactPersonId(1);
//		searchContactPersonById(contactPerson.getContactPersonId());
//		
//	}

	
	// Implement customer CRUD methods
	
	@Override
	public void addCustomer(Customer customer) throws Exception {


        List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
        customer.setCustomerId(customers.get(0).getCustomerId() + 1);
        entityManager.persist(customer);
        // commit it to database
        entityManager.flush();
	}

	@Override
	public Customer searchCustomerById(int id) throws NullPointerException {
		Customer customer = entityManager.find(Customer.class, id);
		if (customer.getCustomerId() == id) {
			customer.getTags();
			return customer;
		}

		throw new NullPointerException();
		
	}

	@Override
	public List<Customer> getAllCustomers() throws NullPointerException {
		List<Customer> resultList  = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
		return resultList;
	}
	
	// this is used for searchByContactPersonId, return the customers based on the input contactPersonId
	@Override
	public Set<Customer> searchCustomerByContactPerson(ContactPerson contactPerson) throws Exception {
        contactPerson = entityManager.find(ContactPerson.class, contactPerson.getContactPersonId());
        contactPerson.getCustomers().size();
        entityManager.refresh(contactPerson);

        return contactPerson.getCustomers();
	}



	@Override
	public void removeCustomer(int customerId) throws NullPointerException {
		// Fixed, using entity manager to get customer object doesn't work
		// But it worked in the week 4 studio...
		Customer customer = this.searchCustomerById(customerId);
		if (customer != null) {
			entityManager.remove(customer);
		}
		//entityManager.refresh(customer);
	}

	@Override
	public void editCustomer(Customer customer) throws Exception {
		try {
			entityManager.merge(customer);
		} catch (Exception ex) {
			
		}
	}

	
	// Implement ContactPerson CRUD methods
	@Override
	public void addContactPerson(ContactPerson contactPerson) throws Exception {
		List<ContactPerson> contactPeople = entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
		contactPerson.setContactPersonId(contactPeople.get(0).getContactPersonId() + 1);
		entityManager.persist(contactPerson);
        // commit it to database
        entityManager.flush();
	}

	
	@Override
	public ContactPerson searchContactPersonById(int id) throws NullPointerException {
		ContactPerson contactPerson = entityManager.find(ContactPerson.class, id);
		if (contactPerson.getContactPersonId() == id) {
			return contactPerson;
		}
		throw new NullPointerException();
	}
	
	
	@Override
	public List<ContactPerson> getAllContactPeople() throws Exception {
		return entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
	}
	
	
	@Override
	public void editContactPerson(ContactPerson contactPerson) throws Exception {
		try {
			entityManager.merge(contactPerson);
		} catch (Exception ex) {
			
		}
	}
	
	@Override
	public void removeContactPerson(int contactPersonId) throws Exception {
		ContactPerson contactPerson = this.searchContactPersonById(contactPersonId);
		if (contactPerson != null) {
			entityManager.remove(contactPerson);
		}
	}
	
	
	// Implement industry CRUD methods
	@Override
	public void addIndustry(Industry industry) throws Exception {
		List<Industry> industries =  entityManager.createNamedQuery(Industry.GET_ALL_QUERY_NAME).getResultList(); 
		industry.setIndustryId(industries.get(0).getIndustryId() + 1);
        entityManager.persist(industry);
	}

	
	@Override
	public Industry searchIndustryById(int id) throws Exception {
		Industry industry = entityManager.find(Industry.class, id);
		//industry.getTags();
        return industry;
	}

	
	@Override
	public void editIndustry(Industry industry) throws Exception {
		try {
			entityManager.merge(industry);
		} catch (Exception ex) {
			
		}
	}

	
	@Override
	public void removeIndustry(int industryId) throws Exception {
		Industry industry = this.searchIndustryById(industryId);
        if (industry != null) {
            entityManager.remove(industry);
        }
	}

	
    @Override
    public List<Industry> getAllIndustries() throws Exception {
        return entityManager.createNamedQuery(Industry.GET_ALL_QUERY_NAME).getResultList();
    }

    
    // Not in use, put more functionality
	@Override
	public Set<Customer> searchCustomerByIndustry(Industry industry) throws Exception {
		industry = entityManager.find(Industry.class, industry.getIndustryId());
		industry.getCustomers().size();
        entityManager.refresh(industry);
        return industry.getCustomers();
	}
	
	//Criteria API, search based on customerName
	public List<Customer> searchCustomerByName(String name) throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Customer> criteriaQuery = builder.createQuery(Customer.class); 
		Root<Customer> c = criteriaQuery.from(Customer.class); 
		criteriaQuery.select(c).where(builder.equal(c.get("customerName").as(String.class), name));
		// return queryResults customer list of matched results
		List<Customer> queryResults = entityManager.createQuery(criteriaQuery).getResultList();
		return queryResults;
	}
		
	
}
