package ausprinting.cm.controllers;

import ausprinting.cm.repository.entities.ContactPerson;

import ausprinting.cm.mbeans.CustomerManagedBean;
import java.util.ArrayList;
import java.util.List;
import javax.el.ELContext;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = ausprinting.cm.repository.entities.ContactPerson.class, value = "contactPerson")

public class ContactPersonConverter implements Converter {

    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    public List<ContactPerson> contactPersonDB; //= CustomerManagedBean.getAllContactPeople();

    public ContactPersonConverter() {
        try {
            //instantiate CustomerManagedBean
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                    .getELResolver().getValue(elContext, null, "customerManagedBean");

            contactPersonDB = customerManagedBean.getAllContactPeople();
        } catch (Exception ex) {

        }
    }

    //this method is for converting the submitted value (as String) to the contact person object
    //the reason for using this method is, the dropdown box in the xhtml does not capture the contact person object, but the String.
    public ContactPerson getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                int number = Integer.parseInt(submittedValue);

                for (ContactPerson c : contactPersonDB) {
                    if (c.getContactPersonId() == number) {
                        return c;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid contact person"));
            }
        }

        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((ContactPerson) value).getContactPersonId());
        }
    }
}
