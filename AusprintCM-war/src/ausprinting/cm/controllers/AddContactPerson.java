package ausprinting.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ausprinting.cm.mbeans.ContactPersonManagedBean;



@RequestScoped
@Named("addContactPerson")
public class AddContactPerson {

	// @ManagedProperty annotation enables us to inject a managed bean into another managed bean.
		@ManagedProperty(value = "#{contactPersonManagedBean}")
		ContactPersonManagedBean contactPersonManagedBean;
		
		private boolean showForm = true;
		
		private ContactPerson contactPerson;
		
		CustomerApplication app;
		
		 
		public void setContactPerson(ContactPerson contactPerson) {
			this.contactPerson = contactPerson;	
		}
		
		public ContactPerson getContactPerson() {
			return contactPerson;
		}
		
		public boolean isShowForm() {
			return showForm;
		}
		
		public AddContactPerson() {
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			
			app = (CustomerApplication) FacesContext.getCurrentInstance()
					.getApplication()
					.getELResolver()
					.getValue(context, null, "customerApplication");
		
			// instantiate contactPersonManagedBean
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		    contactPersonManagedBean = (ContactPersonManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "contactPersonManagedBean");}

		public void addContactPerson(ContactPerson localContactPerson) {
			// this is local contactPerson, not entity
			try {
				// add this contactPerson to db via EJB
				contactPersonManagedBean.addLocalContactPerson(localContactPerson);
				
				// refresh the list in CustomerApplication bean
				app.searchAllContactPeople();
				
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("ContactPerson has been added succesfully"));
			} catch (Exception ex) {
				
			}
			showForm = true;
		}
}
