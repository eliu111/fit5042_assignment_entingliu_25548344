package ausprinting.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;


@Named(value = "contactPersonController")
@RequestScoped
public class ContactPersonController {
	
	
		// this contactPersonId is the index, don't confuse with the ContactPerson Id
		private int contactPersonId;
		
		public int getContactPersonId() {
			return contactPersonId;
		}
		
		public void setContactPersonId(int contactPersonId) {
			this.contactPersonId = contactPersonId;
		}
		
		private ausprinting.cm.repository.entities.ContactPerson contactPerson;
		
		public ContactPersonController()	{
			// Assign contactPerson identifier via GET param
			// this contactPersonId is the index, don't confuse with the ContactPerson Id
			contactPersonId = Integer.valueOf(FacesContext.getCurrentInstance()
					.getExternalContext()
					.getRequestParameterMap()
					.get("contactPersonID"));
			
			// Assign contactPerson based on the id provided
			contactPerson = getContactPerson();
		}
		
		
		public ausprinting.cm.repository.entities.ContactPerson getContactPerson() {
			if (contactPerson == null) {
				// Get application context bean CustomerApplication
				ELContext context = FacesContext.getCurrentInstance().getELContext();
				
				CustomerApplication app = (CustomerApplication) FacesContext.getCurrentInstance()
						.getApplication()
						.getELResolver()
						.getValue(context, null, "customerApplication");
				// -1 to contactPersonId since we +1 in JSF (to always have positive contactPerson id)
				return app.getContactPeople().get(--contactPersonId); // this contactPersonId is the index, don't confuse with the ContactPerson Id
			}
			return contactPerson;
		}
}
