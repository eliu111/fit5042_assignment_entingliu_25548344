package ausprinting.cm.repository;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;

import ausprinting.cm.repository.entities.ContactPerson;
import ausprinting.cm.repository.entities.Customer;


@Remote
public interface ContactPersonRepository {
	/**
	 * Add the contact person being passed as parameter into the repository
	 * 
	 * @param contactPerson
	 * @throws Exception
	 */
	
	public void addContactPerson(ContactPerson contactPerson) throws Exception;
	
	
	/**
	 * Search for a contact person by its contactPerson ID
	 * 
	 * @param id, the contactPersonId of the contactPerson to search for
	 * @return the contact person found
	 * @throws Exception
	 */
	
	public ContactPerson searchContactPersonById(int id) throws Exception;
	
	
	
	/**
	 * Return all the contact people in the repository
	 * 
	 * @return all contact person in the repository
	 * @throws Exception
	 */
	
	public List<ContactPerson> getAllContactPeople() throws Exception;
	
	/**
	 * Remove the contact person, whose contactPerson ID matches the one being passed as
     * parameter, from the repository
	 * 
	 * @param contactPersonId
	 */
	
	public void removeContactPerson(int contactPersonId) throws Exception;
	
	/**
	 * Update a contact person in the repository
	 * 
	 * @param customer
	 * @throws Exception
	 */
	
	public void editContactPerson(ContactPerson contactPerson) throws Exception;
	
	
}
