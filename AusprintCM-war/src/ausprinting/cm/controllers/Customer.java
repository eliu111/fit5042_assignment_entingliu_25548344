package ausprinting.cm.controllers;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.Column;

import ausprinting.cm.repository.entities.Address;
import ausprinting.cm.repository.entities.ContactPerson;
import ausprinting.cm.repository.entities.Industry;

@Named(value = "customer")
@RequestScoped
public class Customer implements Serializable {
	
	// customer attribute
	private int customerId;
	private String customerName;
	private String customerPhoneNumber;
	private String email;
	private Industry industry;
	
	private Address address;
	private ContactPerson contactPerson;
	
	private HashSet<String> tags;
	
	// address attribute
	private String streetNumber;
	private String streetAddress;
	private String suburb;
	private String postcode;
	private String state;
	private String country;
	
	// contact person attribute
	private int contactPersonId;
	private String contactPersonName;
	private String phoneNumber;
	private String contactPersonEmail;
	
	private Set<ausprinting.cm.repository.entities.Customer> customers;

	
	public Customer() {
		this.tags = new HashSet<>();
	}
	
	// non-default constructor
	public Customer(int customerId, String customerName, String customerPhoneNumber, String email, Industry industry, Address address,
			ContactPerson contactPerson, Set<String> tags) {

		this.customerId = customerId;
		this.customerName = customerName;
		this.customerPhoneNumber = customerPhoneNumber;
		this.email = email;
		this.industry = industry;
		this.address = address;
		this.contactPerson = contactPerson;
		this.tags = (HashSet<String>) tags;
	}
	
	// =====================================================================================================
	// Address getter and setter
	public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	
	
	// contact person getter and setter
	public int getContactPersonId() {
		return contactPersonId;
	}

	public void setContactPersonId(int contactPersonId) {
		this.contactPersonId = contactPersonId;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getContactPersonEmail() {
		return contactPersonEmail;
	}

	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}
	
	

	
	// =====================================================================================================
	// customer getter and setter
	public int getCustomerId() {
        return customerId;
    }
    
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public String getcustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public void setcustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}
	
	public Set<ausprinting.cm.repository.entities.Customer> getCustomers(){
		return customers;
	}
	
	public void setCustomers(Set<ausprinting.cm.repository.entities.Customer> customers) {
		this.customers = customers;
	}
	
	
//	// get contactPerson(s)
//	public Set<ausprinting.cm.repository.entities.ContactPerson> getContactPersons(){
//		return contactPersons;
//	}
//	
//	public void setCustomers(Set<ausprinting.cm.repository.entities.Customer> customers) {
//		this.customers = customers;

		
	// get contact person object
	public ContactPerson getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}
	
	public HashSet<String> getTags() {
		return tags;
	}

	public void setTags(HashSet<String> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", customerName=" + customerName + ", customerPhoneNumber="
				+ customerPhoneNumber + ", email=" + email + ", contactPersonEmail=" + contactPersonEmail + ", address=" + address
				+ ", contactPerson=" + contactPerson + ", tags=" + tags + "]";
	}
	

	
}

	