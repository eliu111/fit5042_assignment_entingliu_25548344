package ausprinting.cm.controllers;

import java.io.IOException;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ausprinting.cm.mbeans.CustomerManagedBean;

import java.util.logging.Level;
import java.util.logging.Logger;

import java.io.Serializable;
import java.security.Principal;


@Named(value = "userManager")
@SessionScoped
public class UserManager implements Serializable {
	
	public void logout() throws IOException {
	    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	    ec.invalidateSession();
	    ec.redirect(ec.getRequestContextPath() + "/faces/index.xhtml");
	}
	
//    public String getCurrentUser() {
//    
//	    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//	    Object currentUser;
//		if (ec.getUserPrincipal() == null){
//	    	Logger.info("NO USER LOGGED IN");
//	    }
//	    else{ 
//	        Integer userId = Integer.parseInt(ec.getUserPrincipal().getName());
//	        
//	        try {
//	            currentUser = getLoginService().getLoginById(userId);
//	        }
//	        catch (Exception ex) {
//	        } 
//	    } 
//	    return currentUser;
//    }
}
