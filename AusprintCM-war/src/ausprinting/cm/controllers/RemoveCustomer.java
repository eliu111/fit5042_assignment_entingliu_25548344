package ausprinting.cm.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ausprinting.cm.mbeans.CustomerManagedBean;

/**
 * 
 * @author EnTing
 *
 */

@RequestScoped
@Named(value = "removeCustomer")
public class RemoveCustomer {
	
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;
    
    private Boolean showForm = true;
    
    
    private Customer customer;
    
    CustomerApplication app;
    
    public void setCustomer(Customer customer) {
    	this.customer = customer;
    }
    
    public Customer getCustomer() {
    	return customer;
    }
    
    public boolean isShowForm() {
    	return showForm;
    }
    
    public RemoveCustomer() {
    	ELContext context = FacesContext.getCurrentInstance().getELContext();
    	
    	
    	app = (CustomerApplication) FacesContext.getCurrentInstance()
    			.getApplication()
    			.getELResolver()
    			.getValue(context, null, "customerApplication");
    	
    	app.updateCustomerList();
    	
    	// instantiate customerManagedBean
    	ELContext elContext = FacesContext.getCurrentInstance().getELContext();
    	customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance()
    			.getApplication().getELResolver().getValue(elContext, null, "customerManagedBean");
    }
    
    /**
     * 
     * @param customer Id
     */
    
    public void removeCustomer(int customerId) {
    	try {
    		// remove this customer from db via EJB
    		customerManagedBean.removeCustomer(customerId);
    		
    		//refresh the list in CustomerApplication bean
            app.searchAllCustomers();

    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been deleted succesfully"));
    	} catch (Exception ex) {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to remove customer return at RemoveCustomer!"));
    	}
    	showForm = true;
    }
}
