package ausprinting.cm.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Industry")
@NamedQueries({
@NamedQuery(name = Industry.GET_ALL_QUERY_NAME, query = "SELECT i FROM Industry i order by i.industryId desc")})
public class Industry implements Serializable{
	
	public static final String GET_ALL_QUERY_NAME = "Industry.getAll";
	
	private int industryId;
	private String industryName;
	
	private Set<Customer> customers;
	
	public Industry() {
		
	}

	/**
	 * @param industryId
	 * @param industryName
	 * @param industries
	 */
	public Industry(int industryId, String industryName) {
		super();
		this.industryId = industryId;
		this.industryName = industryName;
		this.customers = new HashSet<>();
	}
	
	
	@Id
    @GeneratedValue
    @Column(name = "industry_id")
	public int getIndustryId() {
		return industryId;
	}

	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}
	
	@OneToMany(mappedBy = "industry")
	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}
	
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + this.industryId;
        return hash;
    }
    
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Industry other = (Industry) obj;
        if (this.industryId != other.industryId) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "Industry [industryId=" + industryId + ", industryName=" + industryName + ", customers=" + customers
				+ "]";
	}
	
	
}
