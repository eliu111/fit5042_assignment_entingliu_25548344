package ausprinting.cm.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.el.ELContext;


// Use javax.enterprise.context for @Named
//import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
// Use javax.faces.bean for @ManagedBean(name ="example")
//import javax.faces.bean.ApplicationScoped;
//import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import ausprinting.cm.mbeans.ContactPersonManagedBean;
import ausprinting.cm.mbeans.CustomerManagedBean;
import ausprinting.cm.repository.entities.ContactPerson;
import ausprinting.cm.repository.entities.Customer;


/**
 * This class is a demonstration of how the application scope works.
 * 
 * @author EnTing
 *
 */

@Named(value = "customerApplication")
@SessionScoped
public class CustomerApplication implements Serializable {
	
	// Dependency injection of managed bean here so that we can use its methods
	@ManagedProperty(value = "#{customerManagedBean}")
	CustomerManagedBean customerManagedBean;
	
	@ManagedProperty(value = "#{contactPersonManagedBean}")
	ContactPersonManagedBean contactPersonManagedBean;
	
	private ArrayList<Customer> customers;
	
	private ArrayList<ContactPerson> contactPeople;
	
	private ArrayList<String> industries;
	
	private boolean showForm = true;
	
	public boolean isShowForm() {
		return showForm;
	}
	
	


	// Add some customer data form db to app
	public CustomerApplication() throws Exception {
		customers = new ArrayList<>();
		contactPeople = new ArrayList<>();
		//industries = new ArrayList<>();
		
		
		
		// instantiate customerManagedBean
		ELContext elcontext = FacesContext.getCurrentInstance().getELContext();
		customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance()
				.getApplication()
				.getELResolver()
				.getValue(elcontext, null, "customerManagedBean");
		
		// instantiate contactPersonManagedBean
		ELContext elcontextContactPerson = FacesContext.getCurrentInstance().getELContext();
		contactPersonManagedBean = (ContactPersonManagedBean) FacesContext.getCurrentInstance()
				.getApplication()
				.getELResolver()
				.getValue(elcontextContactPerson, null, "contactPersonManagedBean");
		
		// get customers from db
		updateCustomerList();
		updateContactPersonList();
		//updateIndustryList();
	}
	
	
	
	// getter and setter for customer arraylist
	public ArrayList<Customer> getCustomers() {
		return customers;
	}
	
	
    private void setCustomers(ArrayList<Customer> newCustomers) {
        this.customers = newCustomers;
    }
    
    
    // getter and setter for contact person arraylist
	public ArrayList<ContactPerson> getContactPeople() {
		return contactPeople;
	}
	
    
    private void setContactPeople(ArrayList<ContactPerson> newContactPeople) {
        this.contactPeople = newContactPeople;
    }


    public void searchCustomerById(int customerId) {
    	try {
    		customers.clear();
        	customers.add(customerManagedBean.searchCustomerById(customerId));
    	} catch (Exception ex) {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The customer id does not exist, please enter a valid id 1"));
    	}
    	
    }
    
    
    public void searchContactPersonById(int contactPersonId) {
    	try {
    		contactPeople.clear();
			contactPeople.add(contactPersonManagedBean.searchContactPersonById(contactPersonId));
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to find contact person by id"));
		}
    }
    
    
    public void searchCustomerByContactPersonId(int contactPersonId) {
        customers.clear();
        Set<Customer> customersByContactPerson = customerManagedBean.searchCustomerByContactPersonId(contactPersonId);
        for (Customer customer : customersByContactPerson) {
        	customers.add(customer);
        }

    }  
    
    
    public void searchPropertyByName(String name) {
        customers.clear();
        for (Customer customer : customerManagedBean.searchCustomerByName(name)) {
            customers.add(customer);
        }
        setCustomers(customers);
    }
    

    public void searchAllCustomers() {
    	try {
    		customers.clear();
        	for (ausprinting.cm.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
        	{
        		customers.add(customer);
        	}
        	setCustomers(customers);
    	} catch (Exception ex) {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to retrieve customer from customerManagedBean.getAllCustomers()"));
    	}
    }
    
    
    public void searchAllContactPeople() {
    	try {
    		contactPeople.clear();
        	for (ausprinting.cm.repository.entities.ContactPerson contactPerson : contactPersonManagedBean.getAllContactPeople())
        	{
        		contactPeople.add(contactPerson);
        	}
        	setContactPeople(contactPeople);
    	} catch (Exception ex) {
    		Logger.getLogger(ContactPersonManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to retrieve all contact people from contactPersonManagedBean.getAllCustomers()"));
    	}
    }
    
    
    // when loading, and after adding or deleting, the customer list needs to be refreshed
    // this method is for that purpose
    
    public void updateCustomerList() {
    	
    	if (customers != null && customers.size() > 0)
    	{
    		
    	}
    	else
    	{
    		customers.clear();
    		
    		try {
				for (ausprinting.cm.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
				{
					customers.add(customer);
				}
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to getAllCustomers()"));
			}
    		
    		setCustomers(customers);
    	}
    }
    
    
    public void updateContactPersonList() {
    	
    	if (contactPeople != null && contactPeople.size() > 0)
    	{
    		
    	}
    	else
    	{
    		contactPeople.clear();
    		
    		try {
				for (ausprinting.cm.repository.entities.ContactPerson contactPerson : contactPersonManagedBean.getAllContactPeople())
				{
					contactPeople.add(contactPerson);
				}
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				Logger.getLogger(ContactPersonManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to retrieve contactPerson from contactPersonManagedBean.getAllContactPeople()"));
			}
    		
    		setContactPeople(contactPeople);
    	}
    }

	
}
