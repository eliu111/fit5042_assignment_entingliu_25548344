package ausprinting.cm.controllers;


import java.util.logging.Level;
import java.util.logging.Logger;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ausprinting.cm.mbeans.ContactPersonManagedBean;

/**
 * 
 * @author EnTing
 *
 */

@RequestScoped
@Named(value = "removeContactPerson")
public class RemoveContactPerson {
	
    @ManagedProperty(value = "#{contactPersonManagedBean}")
    ContactPersonManagedBean contactPersonManagedBean;
    
    private Boolean showForm = true;
    
    
    private ContactPerson contactPerson;
    
    CustomerApplication app;
    
    public void setContactPerson(ContactPerson contactPerson) {
    	this.contactPerson = contactPerson;
    }
    
    public ContactPerson getContactPerson() {
    	return contactPerson;
    }
    
    public boolean isShowForm() {
    	return showForm;
    }
    
    public RemoveContactPerson() {
    	ELContext context = FacesContext.getCurrentInstance().getELContext();
    	
    	
    	app = (CustomerApplication) FacesContext.getCurrentInstance()
    			.getApplication()
    			.getELResolver()
    			.getValue(context, null, "customerApplication");
    	
    	app.updateContactPersonList();
    	
    	// instantiate contactPersonManagedBean
    	ELContext elContext = FacesContext.getCurrentInstance().getELContext();
    	contactPersonManagedBean = (ContactPersonManagedBean) FacesContext.getCurrentInstance()
    			.getApplication().getELResolver().getValue(elContext, null, "contactPersonManagedBean");
    }
    
    /**
     * 
     * @param contactPerson Id
     */
    
    public void removeContactPerson(int contactPersonId) {
    	try {
    		// remove this contactPerson from db via EJB
    		contactPersonManagedBean.removeContactPerson(contactPersonId);
    		
    		//refresh the list in CustomerApplication bean
            app.searchAllContactPeople();;

    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("ContactPerson has been deleted succesfully"));
    	} catch (Exception ex) {
    		Logger.getLogger(ContactPersonManagedBean.class.getName()).log(Level.SEVERE, null, ex);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to remove contactPerson return!"));
    	}
    	showForm = true;
    }
}
