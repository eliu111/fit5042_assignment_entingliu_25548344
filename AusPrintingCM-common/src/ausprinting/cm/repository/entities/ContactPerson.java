package ausprinting.cm.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author En-Ting
 *
 */


@Entity
@Table(name = "CONTACT_PERSON")
@NamedQueries({
@NamedQuery(name = ContactPerson.GET_ALL_QUERY_NAME, query = "SELECT cp FROM ContactPerson cp order by cp.contactPersonId desc")})
public class ContactPerson implements Serializable{
	
	public static final String GET_ALL_QUERY_NAME = "ContactPerson.getAll";
	
	private int contactPersonId;
	private String contactPersonName;
	private String phoneNumber;
	private String contactPersonEmail;
	
	private Set<Customer> customers;
	
	public ContactPerson() {
	}

	/**
	 * @param contactPersonId
	 * @param contactPersonName
	 * @param phoneNumber
	 * @param customers
	 */
	public ContactPerson(int contactPersonId, String contactPersonName, String phoneNumber, String contactPersonEmail) {
		this.contactPersonId = contactPersonId;
		this.contactPersonName = contactPersonName;
		this.phoneNumber = phoneNumber;
		this.contactPersonEmail = contactPersonEmail;
		this.customers = new HashSet<>();
	}

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "contact_person_id")
	public int getContactPersonId() {
		return contactPersonId;
	}

	public void setContactPersonId(int contactPersonId) {
		this.contactPersonId = contactPersonId;
	}

	@Column(name = "contact_person_name")
	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	@Column(name = "phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	
	@Column(name = "contact_person_email")
	public String getContactPersonEmail() {
		return contactPersonEmail;
	}

	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}

	// enforce the relationship between a customer and its contact person using annotation(s). 
	// Each customer has one and only one contact person. Each contact person might be responsible for zero to many customers
    // customer is mapped by contactPerson.
    // inverse table
    // one customer has one and only one contact person.
    // One contactPerson may manage zero to more customers.
	@OneToMany(mappedBy = "contactPerson")
	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	@Override
	// Different from the studio sample
	public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.contactPersonId;
        return hash;
	}

	@Override
	public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContactPerson other = (ContactPerson) obj;
        if (this.contactPersonId != other.contactPersonId) {
            return false;
        }
        return true;
	}

//	@Override
//	public String toString() {
//		return "contactPersonId: ";
//	}
	
	@Override
	public String toString() {
		return "contactPersonId: " + contactPersonId + ", contactPersonName: " + contactPersonName + 
				", phoneNumber: " + phoneNumber + ", contactPersonEmail: " + contactPersonEmail;
	}

	

	
}
