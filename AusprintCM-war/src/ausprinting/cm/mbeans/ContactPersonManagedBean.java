package ausprinting.cm.mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import ausprinting.cm.repository.ContactPersonRepository;
import ausprinting.cm.repository.CustomerRepository;
import ausprinting.cm.repository.entities.Address;
import ausprinting.cm.repository.entities.ContactPerson;





@ManagedBean(name = "contactPersonManagedBean")
@SessionScoped
public class ContactPersonManagedBean implements Serializable {
	
	@EJB
	CustomerRepository customerRepository;
	
	
	/**
	 * Creates a new instance of ContactPersonManagedBean
	 */
	
	public ContactPersonManagedBean() {
	}
	
	
	/**
	 * Add a contact person
	 * @param contactPerson
	 */
	public void addContactPerson(ContactPerson contactPerson) {
		try {
			customerRepository.addContactPerson(contactPerson);;
		} catch (Exception ex) {
			Logger.getLogger(ContactPersonManagedBean.class.getName()).log(Level.SEVERE, null, ex);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("ContactPerson cannot be added!"));
		}
	}
	
	
	/**
	 * Search a contact person by id
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public ContactPerson searchContactPersonById(int id) throws NullPointerException {
		try {
			return customerRepository.searchContactPersonById(id);
		} catch (Exception e) {
			Logger.getLogger(ContactPersonManagedBean.class.getName()).log(Level.SEVERE, null, e);
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The contact id does not exist, please enter a valid id"));
		}
		return null;
	}
	
	
	/**
	 * Update contact person
	 * @param contactPerson
	 */
	public void editContactPerson(ContactPerson contactPerson) {
		try {
			customerRepository.editContactPerson(contactPerson);;
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact Person has been updated succesfully"));
		} catch (Exception ex) {
			Logger.getLogger(ContactPersonManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	
	/**
	 * Delete contact person
	 * @param contactPersonId
	 * @throws Exception
	 */
	public void removeContactPerson(int contactPersonId) throws Exception {
		try {
			customerRepository.removeContactPerson(contactPersonId);;
		} catch (Exception ex) {
			Logger.getLogger(ContactPersonManagedBean.class.getName()).log(Level.SEVERE, null, ex);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to remove the contactPerson! (at ContactPersonManagedBean)"));
		}
	}
	
	
    public List<ContactPerson> getAllContactPeople() throws Exception {
    	List<ContactPerson> contactPeople = customerRepository.getAllContactPeople();
    	return contactPeople;
    }
	
	
	public void addLocalContactPerson(ausprinting.cm.controllers.ContactPerson localContactPerson) {
		// convert this newContactPerson which is the local contactPerson to entity contactPerson
		ContactPerson contactPerson = convertContactPersonToEntity(localContactPerson);
		
		try {
			customerRepository.addContactPerson(contactPerson);
		} catch (Exception ex) {
			Logger.getLogger(ContactPersonManagedBean.class.getName()).log(Level.SEVERE, null, ex);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to convert and add localContactPerson!"));
		}
	}
	
	
	// Convert contactPerson to entity
		public ContactPerson convertContactPersonToEntity(ausprinting.cm.controllers.ContactPerson localContactPerson) {
			ContactPerson contactPerson = new ContactPerson(); //Entity
			
			// ContactPerson attributes
	        int contactPersonId = localContactPerson.getContactPersonId();
	        // name refers to contactPerson name
	        String contactPersonName = localContactPerson.getContactPersonName();
	        String phoneNumber = localContactPerson.getPhoneNumber();
	        String contactPersonEmail = localContactPerson.getContactPersonEmail();
	
	        
	        // Set contactPerson attributes
	        contactPerson.setContactPersonId(contactPersonId);
	        contactPerson.setContactPersonName(contactPersonName);
	        contactPerson.setPhoneNumber(phoneNumber);
	        contactPerson.setContactPersonEmail(contactPersonEmail);
	        
	        
	        return contactPerson;
	}
}
