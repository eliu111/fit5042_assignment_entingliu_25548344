package ausprinting.cm.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ausprinting.cm.mbeans.CustomerManagedBean;

// override the name of the bean by adding a different name to the qualifier
// e.g. @Name("addCustomer")

@RequestScoped
@Named("addCustomer")
public class AddCustomer {
	
	
	// @ManagedProperty annotation enables us to inject a managed bean into another managed bean.
	@ManagedProperty(value = "#{customerManagedBean}")
	CustomerManagedBean customerManagedBean;
	
	private boolean showForm = true;
	
	private Customer customer;
	
	CustomerApplication app;
	
	 
	public void setCustomer(Customer customer) {
		this.customer = customer;	
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public boolean isShowForm() {
		return showForm;
	}
	
	public AddCustomer() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		
		app = (CustomerApplication) FacesContext.getCurrentInstance()
				.getApplication()
				.getELResolver()
				.getValue(context, null, "customerApplication");
	
		// instantiate customerManagedBean
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	    customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "customerManagedBean");}

	public void addCustomer(Customer localCustomer) {
		// this is local customer, not entity
		try {
			// add this customer to db via EJB
			customerManagedBean.addLocalCustomer(localCustomer);
			
			// refresh the list in CustomerApplication bean
			app.searchAllCustomers();
			
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been added succesfully"));
		} catch (Exception ex) {
			
		}
		showForm = true;
	}
}
