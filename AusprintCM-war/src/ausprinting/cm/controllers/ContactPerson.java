package ausprinting.cm.controllers;

import java.io.Serializable;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named(value = "contactPerson")
@RequestScoped
public class ContactPerson implements Serializable {
	
	
	// contact person attribute
	private int contactPersonId;
	private String contactPersonName;
	private String phoneNumber;
	private String contactPersonEmail;
	
	
	private Set<ausprinting.cm.repository.entities.ContactPerson> contactPeople;
	
	
	public ContactPerson() {
	}


	// non default constructor
	public ContactPerson(int contactPersonId, String contactPersonName, String phoneNumber, String contactPersonEmail) {
		this.contactPersonId = contactPersonId;
		this.contactPersonName = contactPersonName;
		this.phoneNumber = phoneNumber;
		this.contactPersonEmail = contactPersonEmail;
	}
	
	
	
	// contact person getter and setter
		public int getContactPersonId() {
			return contactPersonId;
		}

		public void setContactPersonId(int contactPersonId) {
			this.contactPersonId = contactPersonId;
		}

		public String getContactPersonName() {
			return contactPersonName;
		}

		public void setContactPersonName(String contactPersonName) {
			this.contactPersonName = contactPersonName;
		}

		public String getPhoneNumber() {
			return phoneNumber;
		}

		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		
		public String getContactPersonEmail() {
			return contactPersonEmail;
		}

		public void setContactPersonEmail(String contactPersonEmail) {
			this.contactPersonEmail = contactPersonEmail;
		}
		
}
