package ausprinting.cm.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import ausprinting.cm.mbeans.CustomerManagedBean;
import javax.faces.bean.ManagedProperty;

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
	private boolean showForm = true;
	
	private Customer customer;
	
	CustomerApplication app;
	
	private int searchByInt;
	private int contactPersonId;
	private String searchByName;
	private String customerName;
	
	
	public CustomerApplication getApp() {
		return app;
	}
	
	public void setApp(CustomerApplication app) {
		this.app = app;
	}
	
	
    public int getSearchByInt() {
        return searchByInt;
    }
    
    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }
    
    
    public void setCustomer(Customer customer) {
    	this.customer = customer;
    }
    
    public Customer getCustomer() {
    	return customer;
    }
    
    
    public int getContactPersonId() {

        return contactPersonId;
    }

    
    public void setContactPersonId(int contactPersonId) {
        this.contactPersonId = contactPersonId;
    }
    
    
    public String getSearchByName() {
    	
        return searchByName;
    }

    
    public void setSearchByName(String searchByName) {
        this.searchByName = searchByName;
    }
    
    
    public boolean isShowForm() {
    	return showForm;
    }
    
    
    
    public SearchCustomer() {
    	ELContext context = FacesContext.getCurrentInstance().getELContext();
    	
    	app = (CustomerApplication) FacesContext.getCurrentInstance()
    			.getApplication()
    			.getELResolver()
    			.getValue(context, null, "customerApplication");
    	
    	app.updateCustomerList();
    }
    
    
    // CHANGED: add NullPointerException
    public void searchCustomerById(int customerId) {
    	// search this customer then refresh the list
    	try {
    		app.searchCustomerById(customerId);
        	
    	} catch (Exception ex) {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The customer id does not exist, please enter a valid id"));
    	}
    	showForm = true;
    }
    
    
    public void searchAllCustomers() {
    	try {
    		// return all customers from db via EJB
    		app.searchAllCustomers();
    	} catch (Exception ex) {
    		
    	}
    	showForm = true;
    }
    
    // search customer by contact person
    public void searchCustomerByContactPersonId(int contactPersonId) {
        try {
            //search all customers by contact person from db via EJB 
            app.searchCustomerByContactPersonId(contactPersonId);
        } catch (Exception ex) {
        }
        showForm = true;
    }
    
    public void searchCustomerByName(String name) 
    {
       try
       {
            //search this property from db via EJB
            app.searchPropertyByName(name);
       }
       catch (Exception ex)
       {
       }
       showForm = true;
    }
    
}
