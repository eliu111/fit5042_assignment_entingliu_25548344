package ausprinting.cm.controllers;

import ausprinting.cm.repository.entities.Industry;

import ausprinting.cm.mbeans.CustomerManagedBean;
import java.util.ArrayList;
import java.util.List;
import javax.el.ELContext;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = ausprinting.cm.repository.entities.Industry.class, value = "industry")

public class IndustryConverter implements Converter {

    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    public List<Industry> industryDB; //= customerManagedBean.getAllIndustries();

    public IndustryConverter() {
        try {
            //instantiate customerManagedBean
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                    .getELResolver().getValue(elContext, null, "customerManagedBean");

            industryDB = customerManagedBean.getAllIndustries();
        } catch (Exception ex) {

        }
    }

    //this method is for converting the submitted value (as String) to the contact person object
    //the reason for using this method is, the dropdown box in the xhtml does not capture the contact person object, but the String.
    public Industry getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                int number = Integer.parseInt(submittedValue);

                for (Industry i : industryDB) {
                    if (i.getIndustryId() == number) {
                        return i;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid industry"));
            }
        }

        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Industry) value).getIndustryId());
        }
    }
}
