package ausprinting.cm.controllers;


import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "customerController")
@RequestScoped
public class CustomerController {
	
	// this customerId is the index, don't confuse with the Customer Id
	private int customerId;
	
	public int getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	private ausprinting.cm.repository.entities.Customer customer;
	
	public CustomerController()	{
		// Assign customer identifier via GET param after user submit the form
		// this customerID is the index, don't confuse with the Customer Id
		customerId = Integer.valueOf(FacesContext.getCurrentInstance()
				.getExternalContext()
				.getRequestParameterMap()
				.get("customerID"));
		
		// Assign customer based on the id provided
		customer = getCustomer();
	}
	
	
	public ausprinting.cm.repository.entities.Customer getCustomer(){
		if (customer == null) {
			// Get application context bean CustomerApplication
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			
			CustomerApplication app = (CustomerApplication) FacesContext.getCurrentInstance()
					.getApplication()
					.getELResolver()
					.getValue(context, null, "customerApplication");
			// -1 to customerId since we +1 in JSF (to always have positive customer id)
			return app.getCustomers().get(--customerId); // this customerId is the index, don't confuse with the Customer Id
		}
		return customer;
	}
	
}
