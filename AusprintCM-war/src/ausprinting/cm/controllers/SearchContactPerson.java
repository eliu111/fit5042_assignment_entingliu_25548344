package ausprinting.cm.controllers;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import ausprinting.cm.mbeans.CustomerManagedBean;
import javax.faces.bean.ManagedProperty;

@RequestScoped
@Named("searchContactPerson")
public class SearchContactPerson {
	private boolean showForm = true;
	
	
	private ContactPerson contactPerson;
	
	
	CustomerApplication app;
	
	
	private int searchByInt;
	
	
	public CustomerApplication getApp() {
		return app;
	}
	
	public void setApp(CustomerApplication app) {
		this.app = app;
	}
	
	
    public int getSearchByInt() {
        return searchByInt;
    }
    
    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }
    
    
    public void setContactPerson(ContactPerson contactPerson) {
    	this.contactPerson = contactPerson;
    }
    
    public ContactPerson getContactPerson() {
    	return contactPerson;
    }
    
    public boolean isShowForm() {
    	return showForm;
    }
    
    public SearchContactPerson() {
    	ELContext context = FacesContext.getCurrentInstance().getELContext();
    	
    	app = (CustomerApplication) FacesContext.getCurrentInstance()
    			.getApplication()
    			.getELResolver()
    			.getValue(context, null, "customerApplication");
    	
    	app.updateContactPersonList();
    }
    
    
    // CHANGED: add NullPointerException
    public void searchContactPersonById(int contactPersonId) {
    	// search this contact person then refresh the list in CustomerApplication bean
    	try {
    		app.searchContactPersonById(contactPersonId);
        	
    	} catch (Exception ex) {
    		
    	}
    	showForm = true;
    }
    

    // Need change of searchall
    public void searchAllContactPeople() {
    	try {
    		// return all contact person from db via EJB
    		app.searchAllContactPeople();
    	} catch (Exception ex) {
    		
    	}
    	showForm = true;
    }
}
