package ausprinting.cm.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
//import javax.enterprise.context.ApplicationScoped;
//import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
//import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import ausprinting.cm.repository.ContactPersonRepository;
import ausprinting.cm.repository.CustomerRepository;
import ausprinting.cm.repository.entities.Address;
import ausprinting.cm.repository.entities.ContactPerson;
import ausprinting.cm.repository.entities.Customer;
import ausprinting.cm.repository.entities.Industry;


@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable{
	
	// @EJB: Injects a reference of the local, remote, or no-interface view of an EJB into the annotated variable.
	@EJB
	CustomerRepository customerRepository;
	
	@PostConstruct
	public void init() {
		Customer customer = new Customer();
		customer.setCustomerId(1);
		searchCustomerById(customer.getCustomerId());
		
	}

	/**
	 * Creates a new instance of CustomerManagedBean
	 */
	
	public CustomerManagedBean() {
		
	}
	
	// =====================Customer=================
	
	/**
	 * Add a new customer
	 * @param customer
	 */
	public void addCustomer(Customer customer) {
		try {
			customerRepository.addCustomer(customer);
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer cannot be added!"));
		}
	}
	
	
	/**
	 * Search a customer by Id
	 * @throws Exception 
	 * 
	 */
	public Customer searchCustomerById(int id) throws NullPointerException {
			try {
				return customerRepository.searchCustomerById(id);
			} catch (Exception e) {
				Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
	    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The customer id does not exist, please enter a valid id 2"));
			}
			return null;
	}
	
	
	/**
	 *  Question
	 *  
	 * @param customer
	 */
    
	public void editCustomer(Customer customer) throws NullPointerException{
		try {
			String s = customer.getAddress().getStreetNumber();
			Address address = customer.getAddress();
			address.setStreetNumber(s);
			customer.setAddress(address);
			
			customerRepository.editCustomer(customer);
			
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	
	public void removeCustomer(int customerId) throws NullPointerException {
		try {
			customerRepository.removeCustomer(customerId);
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to remove customer return at CustomerManagedBean!"));
		}
	}
	
	
	// CHANGED for nullPointerException
	public List<Customer> getAllCustomers() throws Exception {
		List<Customer> customers = customerRepository.getAllCustomers();
		return customers;
	}
	
	
	// This is used for calling the entity manager to search the customer by customerName
    public List<Customer> searchCustomerByName(String name) {
        try {
            return customerRepository.searchCustomerByName(name);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
	
    
	public void addLocalCustomer(ausprinting.cm.controllers.Customer localCustomer) {
		// convert this newCustomer which is the local customer to entity customer
		Customer customer = convertCustomerToEntity(localCustomer);
		
		try {
			customerRepository.addCustomer(customer);
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to convert and add localCustomer!"));
		}
	}
	
	
	// Convert customer to entity
	public Customer convertCustomerToEntity(ausprinting.cm.controllers.Customer localCustomer) {
		Customer customer = new Customer(); //Entity
		
		
		// Address attributes
        String streetNumber = localCustomer.getStreetNumber();
        String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        String country = localCustomer.getCountry();
        
        
        // Customer attributes
        String customerName = localCustomer.getCustomerName();
        String customerPhoneNumber = localCustomer.getcustomerPhoneNumber();
        String email = localCustomer.getEmail();
        int customerId = localCustomer.getCustomerId();
        
        // Industry attribute
        // Industry industry = new ausprinting.cm.repository.entities.Industry();
        
        
//        // ContactPerson attributes//bug
//        int contactPersonId = localCustomer.getContactPersonId();
//        // name refers to contactPerson name
//        String contactPersonName = localCustomer.getContactPersonName();
//        String phoneNumber = localCustomer.getPhoneNumber();
//        String contactPersonEmail = localCustomer.getContactPersonEmail();
        
        
        // Set address attributes to Address object
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state, country);
        
        
//        // Set contact person attributes
//        ContactPerson contactPerson = new ausprinting.cm.repository.entities.ContactPerson(contactPersonId, contactPersonName, phoneNumber, contactPersonEmail);
//		if (contactPerson.getContactPersonId() == 0){
//			contactPerson = null;
//		}
		

        // Set customer attributes
        customer.setAddress(address);
        customer.setCustomerName(customerName);
        customer.setcustomerPhoneNumber(customerPhoneNumber);
        customer.setEmail(email);
        customer.setIndustry(localCustomer.getIndustry());
        customer.setCustomerId(customerId);
        customer.setTags(localCustomer.getTags());
        customer.setContactPerson(localCustomer.getContactPerson());
        
        
		return customer;
	}
	
	
	// ==================Contact Person================
	
	
    public List<ContactPerson> getAllContactPeople() throws Exception {
        try {
            return customerRepository.getAllContactPeople();
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    // Search Customer by contactPersonId
	public Set<Customer> searchCustomerByContactPersonId(int contactPersonId) {
		try {
            //retrieve contact person by id
            for (ContactPerson contactPerson : customerRepository.getAllContactPeople()) {
                if (contactPerson.getContactPersonId() == contactPersonId) {
                    return customerRepository.searchCustomerByContactPerson(contactPerson);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
	}
	
	// ==================Industry================
	
	public List<Industry> getAllIndustries() throws Exception {
		try {
			return customerRepository.getAllIndustries();
		} catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		return null;
	}


	
}
